import socket
import sys
import time

if len(sys.argv) == 3:
    # Get "IP address of Server" and also the "port number" from    argument1 and argument 2
    ip = sys.argv[1]
    port = int(sys.argv[2])
else:
    print("Run like : python3 server.py <arg1:server ip:this system IP 192.168.1.6> <arg2:server port:4444 >")
    exit(1)

# Create a UDP socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Bind the socket to the port
server_address = (ip, port)
s.bind(server_address)
print("Do Ctrl+c to exit the program !!")


print("####### Server is listening #######")
counter = 0
while True:
    counter += 1
    data, address = s.recvfrom(4096)
    msg_counter, time_send = data.decode('utf-8').split(";")
    print("Server received: ", data.decode('utf-8'))
    response_data = data.decode('utf-8') + ";" + str(counter) +  ";" + str(time.time())
    s.sendto(response_data.encode('utf-8'), address)
    counter = int(msg_counter)
