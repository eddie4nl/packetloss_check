import socket
import sys
import time
import datetime

if len(sys.argv) == 3:
    # Get "IP address of Server" and also the "port number" from argument 1 and argument 2
    ip = sys.argv[1]
    port = int(sys.argv[2])
else:
    print("Run like : python3 client.py <arg1 server ip 192.168.1.102> <arg2 server port 4444 >")
    exit(1)

# Create socket for server
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
s.settimeout(1) # Sets the socket to timeout after 1 second of no activity
print("Do Ctrl+c to exit the program !!")

#
def cal_average_list(num):
    sum_num = 0
    for t in num:
        sum_num = sum_num + t

    avg = sum_num / len(num)
    return avg


# Let's send data through UDP protocol
counter = 0
deviation_time_server = []
roundtrips = [5]
while True:
    counter += 1
    send_data = str(counter)+";"+str(time.time())
    s.sendto(send_data.encode('utf-8'), (ip, port))


    try:
        data, address = s.recvfrom(4096)
        msg_counter,time_send, server_counter, server_send = data.decode('utf-8').split(";")

        if (cal_average_list(roundtrips) * 3) < time.time() - float(time_send):
            print("HIGH RTT " + str((time.time() - float(time_send)) * 1000)[:3] + "ms" +"; "+str(datetime.datetime.now()))

        roundtrips.append((time.time() - float(time_send)))
        #deviation_time_server.append((float(server_send) - (time.time() - float(time_send))) - time.time())
        #avarage_deviation_time_server = cal_average_list(deviation_time_server)
        #print("Time to server " + str(((time.time() - float(server_send)) - avarage_deviation_time_server) * 1000)[:5] + "ms")
        if int(server_counter) != counter:
            print("server_didnt_get " + server_counter + " up to " + str(counter) + " : "+str(datetime.datetime.now()))

        #print("Server response: ", data.decode('utf-8'))
    except socket.timeout:
        print("NO_RESPONSE_packet;"+ str(counter)+"; "+str(datetime.datetime.now()))

    time.sleep(0.1)
# close the socket
s.close()
